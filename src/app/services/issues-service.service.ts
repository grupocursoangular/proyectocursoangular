import { Injectable } from '@angular/core';
import { Issue } from '../model/issue';
import { GitlabApiService } from './gitlab-api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class IssuesServiceService extends GitlabApiService {

  public static ISSUES_SERVICE = '/issues/';

  issue: Issue;
  listIssues: Array<Issue>;
  keystorage = 'listIssues';

  constructor(public httpClient: HttpClient) { super(httpClient); }


  /*
  setIssue (issues:Array<Issue>) {
    this.listIssues = this.getListIssues();
    if (this.listIssues === undefined || this.listIssues== null){
      this.listIssues  = new Array<Issue>();
    }
    this.listIssues.push(issue);
    //localStorage.setItem(this.keystorege, JSON.stringify(this.personas));
    // todo Llamada a grabar Issue de la Api de Git
  }
*/
  saveIssues  (issues: Array<Issue>) {
    this.listIssues = issues;
    localStorage.setItem(this.keystorage, JSON.stringify(this.listIssues));
    // todo Llamada a grabar Issue de la Api de Git
  }
  getListIssues()  {
    return this.httpClient.get<Array<Issue>>(super.getUrlServicio(IssuesServiceService.ISSUES_SERVICE), this.httpOptions);
  }

  findIssue (id: number) {
    this.listIssues = JSON.parse(localStorage.getItem(this.keystorage));
    this.issue = this.listIssues.find (issue => issue.id === id);
    return this.issue;
  }

  createIssue(issue: Issue) {
    return this.httpClient.post<Issue>(super.getUrlServicio(IssuesServiceService.ISSUES_SERVICE) +
       '?title=' + issue.title + '&description=' + issue.description, {}, this.httpOptions);
  }
}
