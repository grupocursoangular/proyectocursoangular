import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class GitlabApiService {

  public static proyectId :number = 9958899;

  public static URL_PROJECT: string = 'https://gitlab.com/api/v4/projects/';

  httpOptions = {
    headers: new HttpHeaders ({
    'Content-Type': 'application/json',
    'PRIVATE-TOKEN': 's7zYsu-MPpajuSHGXUP9'
    })
  };

  constructor(public httpClient: HttpClient) {

  }

  getUrlServicio(servicio: string):string {
    return GitlabApiService.URL_PROJECT + GitlabApiService.proyectId + servicio;
  }


}
