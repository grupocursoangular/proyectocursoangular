import { Injectable } from '@angular/core';
import { GitlabApiService } from './gitlab-api.service';
import { HttpClient } from '@angular/common/http';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})

export class MembersService extends GitlabApiService {

  public static MEMBERS_SERVICE: string = '/members/';

  constructor(public httpClient: HttpClient) { super(httpClient); }

  getUsers() {
      return this.httpClient.get<Array<User>>(super.getUrlServicio(MembersService.MEMBERS_SERVICE), 
        this.httpOptions);
  }
}