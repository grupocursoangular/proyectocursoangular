import { Injectable } from '@angular/core';
import { GitlabApiService } from './gitlab-api.service';
import { HttpClient } from '@angular/common/http';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends GitlabApiService {

  public static USERS_SERVICE = '/users/';

  constructor(public httpClient: HttpClient) { super(httpClient); }

  getUsers() {
      return this.httpClient.get<Array<User>>(super.getUrlServicio(UsersService.USERS_SERVICE), this.httpOptions);
  }
}
