import { Component, OnInit, Input } from '@angular/core';
import { Issue } from 'src/app/model/issue';
import { IssuesServiceService } from 'src/app/services/issues-service.service';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-issue-detail',
  templateUrl: './issue-detail.component.html',
  styleUrls: ['./issue-detail.component.scss']
})
export class IssueDetailComponent implements OnInit {

  issue: Issue;

  assignees: Array <User>;
  labels: Array <string>;

  constructor( route: ActivatedRoute, public servicioIssue: IssuesServiceService ) {
    const id = parseInt (route.snapshot.paramMap.get('id'), 10);
    this.issue = servicioIssue.findIssue(id);
    this.assignees = this.issue.assignees;
    this.labels = this.issue.labels;
  }

  ngOnInit() {
  }

}
