import { Component, OnInit, Input } from '@angular/core';
import { IssuesServiceService } from 'src/app/services/issues-service.service';
import { Issue } from 'src/app/model/issue';
import { RouterLink, ActivatedRoute, Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-issue-list',
  templateUrl: './issue-list.component.html',
  styleUrls: ['./issue-list.component.scss']
})
export class IssueListComponent implements OnInit {

  issues: Array<Issue> = [];
  selectedIssue: Issue;
  issuesServices: IssuesServiceService;

  constructor(private isService: IssuesServiceService, private router: Router) {
      this.issuesServices = isService;
   }

  ngOnInit() {
    this.issuesServices.getListIssues()
    .subscribe(result => {
      this.issues = result;
      this.issuesServices.saveIssues(this.issues);
      console.log(this.issues);
    });
  }

  onSelect(issue: Issue) {
    // console.log('Persona + ' + persona.nombre);
    this.selectedIssue = issue;
    const navigationExtras: NavigationExtras = {
      queryParams: { 'id': this.selectedIssue.id }
    };
    this.router.navigate(['detail/' + this.selectedIssue.id]);
  }

}
