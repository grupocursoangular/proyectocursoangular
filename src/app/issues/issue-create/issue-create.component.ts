import { Component, OnInit } from '@angular/core';
import { Issue } from 'src/app/model/issue';
import { IssuesServiceService } from 'src/app/services/issues-service.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-issue-create',
  templateUrl: './issue-create.component.html',
  styleUrls: ['./issue-create.component.scss']
})
export class IssueCreateComponent implements OnInit {

  issue: Issue;
  users: Array<any> = [];
  userNames: Array<string>;
  msgSuccess = false;
  msgError = false;

  constructor(private issueService: IssuesServiceService, private userService: UsersService) {
    this.issue = new Issue();
    this.getUsers();
  }

  ngOnInit() { }

  createIssue() {
     this.issueService.createIssue(this.issue)
      .subscribe(res => {
        this.msgSuccess = true;
        this.msgError = false;
        this.hideMessages();
      },
      err => {
        this.msgError = true;
        this.msgSuccess = false;
        this.hideMessages();
      });
  }

  getUsers() {
     this.userService.getUsers().subscribe(res => {
      this.users = res;
    });
  }

  hideMessages() {
    const component = this;
    setTimeout(function() {
      component.msgSuccess = false;
      component.msgError = false;
    }, 5000);
  }

}
