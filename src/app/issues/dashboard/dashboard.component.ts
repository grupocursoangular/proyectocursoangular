import { Component, OnInit } from '@angular/core';
import { IssuesServiceService } from 'src/app/services/issues-service.service';
import { Issue } from 'src/app/model/issue';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  private issues: Array<Issue>;
  private chartTypePie: String = 'pie';
  private chartTypeHor: String = 'horizontalBar';

  private datasetsEstado: Array<any>; // = [
//     { data: [6, 1], label: 'Por estado' }
//   ];

  private datasetsAsignados: Array<any>; // = [
  //   { data: [300, 50, 100, 40, 120], label: 'Por asignación' }
  // ];

  private labelsEstados: Array<any>; // = ['Cerradas', 'Abiertas'];

  private colorsEstados: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      hoverBackgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      borderWidth: 2,
    }
  ];

  private labelsAsignados: Array<any>; // = ['Coto', 'Carlos', 'Gamero', 'Gonzalo', 'Juan A.'];

  private colorsAsignados: Array<any> = [
    {
      backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'],
      hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774'],
      borderWidth: 2,
    }
  ];

  private chartOptionsPie: any = {
    responsive: true
  };

  private chartOptionsHor: any = {
    responsive: true,
    scales: {
        xAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
  };

  public estadoClicked(e: any): void { }
  public estadoHovered(e: any): void { }

  public asignadoClicked(e: any): void { }
  public asignadoHovered(e: any): void { }

  constructor(private issuesService: IssuesServiceService) {
    this.issuesService.getListIssues()
    .subscribe(result => {
      this.issues = result;
      this.reloadDashboardData();

      console.log(this.issues);
    });
  }

  private reloadDashboardData() {

    const estados = this.issues.map(issue => issue.state);
    const listaAsignados = new Set();
    this.labelsEstados = Array.from(new Set(estados));
    this.issues.forEach(function (item) {
      item.assignees.forEach(function (asignado) {
        listaAsignados.add(asignado.name);
      });
    });

    const datosEstado: Array<Number> = new Array<Number>();
    const tareas = this;
    this.labelsEstados.forEach(function (item) {
      let contador = 0;
      tareas.issues.forEach(issue => {
        if (issue.state === item) {
          contador++;
        }
      });
      datosEstado.push(contador);
    });

    this.datasetsEstado = [
      { data: datosEstado, label: 'Por estado' }
    ];

    this.labelsAsignados = Array.from(listaAsignados);
    const datosAsignado: Array<Number> = new Array<Number>();
    this.labelsAsignados.forEach(function (item) {
      let contador = 0;
      tareas.issues.forEach(function (issue) {
        issue.assignees.forEach(function (asignado) {
          if (asignado.name === item) {
            contador++;
          }
        });
      });
      datosAsignado.push(contador);
    });

    this.datasetsAsignados = [
      { data: datosAsignado, label: 'Por asignación' }
    ];
  }

  ngOnInit() {
  }

}

