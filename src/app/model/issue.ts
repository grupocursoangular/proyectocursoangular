import { User } from './user';

export class Issue {
    constructor (
        public id?: Number,
        public iid?: Number,
        public project_id?: String,
        public title?: String,
        public description?: String,
        public state?: String,
        public created_at?: Date,
        public update_at?: Date,
        public closed_at?: Date,
        public closed_by?: User,
        public labels?: Array<string>,
        public milestone?: String,
        public assignees?: Array<User>) {}
}
