export class User {
    constructor (
        public id: Number,
        public name: String,
        public username: String,
        public state: String,
        public avatar_url: Date) {}
}
