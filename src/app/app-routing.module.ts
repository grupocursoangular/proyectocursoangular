import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IssueListComponent } from './issues/issue-list/issue-list.component';
import { IssueCreateComponent } from './issues/issue-create/issue-create.component';
import { IssueDetailComponent } from './issues/issue-detail/issue-detail.component';
import { DashboardComponent } from './issues/dashboard/dashboard.component';

const routes: Routes = [
  { path: '', component: IssueListComponent },
  { path: 'new', component: IssueCreateComponent },
  { path: 'detail/:id', component: IssueDetailComponent },
  { path: 'dashboard', component: DashboardComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
